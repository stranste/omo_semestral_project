package cz.cvut.fel.omo.iterators;

import cz.cvut.fel.omo.electronics.sensors.Sensor;
import cz.cvut.fel.omo.house.House;
import cz.cvut.fel.omo.house.Room;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SensorIterator implements Iterator<Sensor> {

    List<Sensor> list;
    int count, index;
    public SensorIterator(House house) {
        list = new ArrayList<Sensor>();
        for (Room room:house.getRooms()) {
            list.addAll(room.getSensors());
        }
        index = 0;
        count = list.size();
    }

    @Override
    public boolean hasNext() {
        return index < count;
    }

    @Override
    public Sensor next() {
        return list.get(index++);
    }
}
