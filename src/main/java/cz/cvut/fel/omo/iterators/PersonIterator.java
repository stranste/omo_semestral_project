package cz.cvut.fel.omo.iterators;

import cz.cvut.fel.omo.house.House;
import cz.cvut.fel.omo.house.Room;
import cz.cvut.fel.omo.inmates.people.Person;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PersonIterator implements Iterator<Person> {

    List<Person> list;
    int count, index;
    public PersonIterator(House house) {
        list = new ArrayList<Person>();
        for (Room room:house.getRooms()) {
            list.addAll(room.getPersons());
        }
        index = 0;
        count = list.size();
    }

    @Override
    public boolean hasNext() {
        return index < count;
    }

    @Override
    public Person next() {
        return list.get(index++);
    }
}
