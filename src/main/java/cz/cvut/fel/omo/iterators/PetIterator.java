package cz.cvut.fel.omo.iterators;

import cz.cvut.fel.omo.house.House;
import cz.cvut.fel.omo.house.Room;
import cz.cvut.fel.omo.inmates.pets.Pet;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class PetIterator implements Iterator<Pet> {

    List<Pet> list;
    int count, index;
    public PetIterator(House house) {
        list = new ArrayList<Pet>();
        for (Room room:house.getRooms()) {
            list.addAll(room.getPets());
        }
        index = 0;
        count = list.size();
    }

    @Override
    public boolean hasNext() {
        return index < count;
    }

    @Override
    public Pet next() {
        return list.get(index++);
    }
}
