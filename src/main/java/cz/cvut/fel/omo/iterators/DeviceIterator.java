package cz.cvut.fel.omo.iterators;

import cz.cvut.fel.omo.electronics.devices.Device;
import cz.cvut.fel.omo.house.House;
import cz.cvut.fel.omo.house.Room;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class DeviceIterator implements Iterator<Device> {

    List<Device> list;
    int count, index;

    public DeviceIterator(House house) {
        list = new ArrayList<Device>();
        for (Room room:house.getRooms()) {
            list.addAll(room.getDevices());
        }
        index = 0;
        count = list.size();
    }

    @Override
    public boolean hasNext() {
        return index < count;
    }

    @Override
    public Device next() {
        return list.get(index++);
    }
}
