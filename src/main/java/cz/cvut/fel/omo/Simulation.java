package cz.cvut.fel.omo;

import com.fasterxml.jackson.databind.ObjectMapper;
import cz.cvut.fel.omo.api.PersonUsable;
import cz.cvut.fel.omo.electronics.devices.Device;
import cz.cvut.fel.omo.electronics.sensors.Sensor;
import cz.cvut.fel.omo.house.Garage;
import cz.cvut.fel.omo.house.House;
import cz.cvut.fel.omo.house.Room;
import cz.cvut.fel.omo.inmates.people.Person;
import cz.cvut.fel.omo.inmates.pets.Pet;
import cz.cvut.fel.omo.iterators.DeviceIterator;
import cz.cvut.fel.omo.iterators.PersonIterator;
import cz.cvut.fel.omo.iterators.PetIterator;
import cz.cvut.fel.omo.iterators.SensorIterator;
import cz.cvut.fel.omo.vehicles.Vehicle;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

public class Simulation {
    private int ticks;
    private House house;
    private int temperature;
    private int humidity;
    private int dayLength;
    HashMap<Room, HashMap<Device, Integer>> consumptions;
    HashMap<Person, HashMap<PersonUsable, Integer>> usage;
    List<List<Object>> events;
    int currentTick = 0;

    /**
     * Constructor for the simulation.
     * @param ticks Number of ticks the simulation will run.
     * @param filename Filename of a json house config file.
     * @param temperature Environment temperature in the simulation.
     * @param humidity Environment humidity in the simulation.
     * @param dayLength The length of a day in simulation ticks.
     */
    public Simulation(int ticks, String filename,  int temperature, int humidity, int dayLength) {
        this.ticks = ticks;
        this.temperature = temperature;
        this.humidity = humidity;
        this.dayLength = dayLength;
        ObjectMapper objectMapper = new ObjectMapper();
        try {
            house = objectMapper.readValue(new File(filename), House.class);
        } catch (IOException e) {
            System.err.println("Cannot read the house-config from the file!");
            e.printStackTrace();
            System.exit(1);
        }
    }

    /**
     * Constructor for the simulation.
     * @param ticks Number of ticks the simulation will run.
     * @param house The house for the simulation.
     * @param temperature Environment temperature in the simulation.
     * @param humidity Environment humidity in the simulation.
     * @param dayLength The length of a day in simulation ticks.
     */
    public Simulation(int ticks, House house, int temperature, int humidity, int dayLength) {
        this.ticks = ticks;
        this.house = house;
        this.temperature = temperature;
        this.humidity = humidity;
        this.dayLength = dayLength;
    }

    /**
     * This method runs the simulation.
     */
    public void run() {
        consumptions = new HashMap<Room, HashMap<Device, Integer>>();
        events = new ArrayList<List<Object>>();
        usage = new HashMap<Person, HashMap<PersonUsable, Integer>>();
        for(Room room:house.getRooms()) {
            HashMap<Device, Integer> consumption = new HashMap<Device, Integer>();
            for(Device device: room.getDevices()) {
                consumption.put(device, 0);
            }
            consumptions.put(room, consumption);
        }
        for (currentTick = 0; currentTick < ticks; currentTick++) {
            runTick();
        }
    }

    private void runTick() {

        updateRoomVars();

        updateEntities();

        updateReports();
    }

    private void updateReports() {
        for(Room room: house.getRooms()) {
            HashMap<Device, Integer> consumption = consumptions.get(room);
            for(Device device: room.getDevices()) {
                Integer currentConsumption = consumption.get(device);
                if(currentConsumption == null) {
                    consumption.put(device, device.getConsumption());
                }
                else {
                    consumption.put(device, device.getConsumption() + currentConsumption);
                }
            }
        }
    }

    private void updateEntities() {
        Iterator<Person> personIterator = new PersonIterator(house);
        while(personIterator.hasNext()) {
            Person person = personIterator.next();
            person.update(house.getRooms());
            updateUsage(person);
        }

        Iterator<Pet> petIterator = new PetIterator(house);
        while(petIterator.hasNext()) {
            petIterator.next().update(house.getRooms());
        }

        Iterator<Sensor> sensorIterator = new SensorIterator(house);
        while(sensorIterator.hasNext()) {
            sensorIterator.next().report();
        }

        for(Room room: house.getRooms()) {
            room.getEventHandler().handleEvents();
            events.add(room.getEventHandler().getAllEvents());
        }

        Iterator<Device> deviceIterator = new DeviceIterator(house);
        while(deviceIterator.hasNext()) {
            deviceIterator.next().update();
        }
    }

    private void updateUsage(Person person) {
        PersonUsable personUsable = person.getDeviceInUse();
        HashMap<PersonUsable, Integer> concreteUsage = usage.get(person);
        if(concreteUsage == null) {
            concreteUsage = new HashMap<PersonUsable, Integer>();
            usage.put(person, concreteUsage);
        }
        Integer usageCounter = concreteUsage.get(personUsable);
        if(usageCounter == null) {
            concreteUsage.put(personUsable, 1);
        }
        else {
            concreteUsage.put(personUsable, usageCounter + 1);
        }
    }

    private void updateRoomVars() {
        for (Room room:house.getRooms()) {
            if(temperature > room.getTemperature()) {
                room.setTemperature(room.getTemperature() + 1);
            }
            else if(temperature < room.getTemperature()) {
                room.setTemperature(room.getTemperature() - 1);
            }

            if(humidity > room.getHumidity()) {
                room.setHumidity(room.getHumidity() + 1);
            }
            else if(humidity < room.getHumidity()) {
                room.setHumidity(room.getHumidity() - 1);
            }

            int lightLevel = currentTick % (dayLength);
            if(lightLevel > (dayLength/2)) {
                lightLevel -= (lightLevel - (dayLength/2)) * 2;
            }
            room.setNaturalLight(lightLevel  * 100 / (dayLength/2));
        }
    }

    /**
     *
     * @return the event report.
     */
    public String eventReport() {
        String report = "Events in house:";
        for(int ii = 0; ii < ticks; ii++) {
            report += "\n  Tick " + ii +":";
            for (int i = 0; i < house.getRooms().size(); i++) {
                for (Object event:events.get(ii * house.getRooms().size() + i))
                report += "\n      " + event;
            }
        }
        return report + "\n";
    }

    /**
     *
     * @return configuration of the house.
     */
    public String houseConfigReport() {
        String report = "House:";
        for (Room room:house.getRooms()) {
            report += "\n  " + room;
            report += "\n    Devices:";
            for (Device device: room.getDevices()) {
                report += "\n      " + device;
            }
            report += "\n    Sensors:";
            for (Sensor sensor: room.getSensors()) {
                report += "\n      " + sensor;
            }
            report += "\n    Persons:";
            for (Person person: room.getPersons()) {
                report += "\n      " + person;
            }
            report += "\n    Pets:";
            for (Pet pet: room.getPets()) {
                report += "\n      " + pet;
            }
            if(room instanceof Garage garage) {
                report += "\n    Vehicles:";
                for (Vehicle vehicle: garage.getVehicles()) {
                    report += "\n      " + vehicle;
                }
            }
        }
        return report + "\n";
    }

    /**
     *
     * @return consumption of all devices in the house.
     */
    public String consumptionReport() {
        String report = "Consumption in house:";
        int houseSum = 0;
        for (Room room: house.getRooms()) {
            int roomSum = 0;
            report += "\n  " + room;
            report += "\n    Devices:";
            for (Device device: room.getDevices()) {
                report += "\n      " + device;
                report += "\n        had consumption of " + consumptions.get(room).get(device) + " watt-hours.";
                roomSum += consumptions.get(room).get(device);
            }
            houseSum += roomSum;
            report += "\n      " + "in total: " + roomSum + " watt-hours." ;
        }
        report += "\n" + "in total: " + houseSum + " watt-hours. Which is approximately: " + (houseSum / 200) + " CZK." ;
        return report + "\n";
    }

    /**
     *
     * @return usages of all persons in the house.
     */
    public String usageReport() {
        String report = "Usage in house:";
        Iterator<Person> personIterator = new PersonIterator(house);
        while(personIterator.hasNext()) {
            Person person = personIterator.next();
            HashMap<PersonUsable, Integer> personUsage = usage.get(person);
            report += "\n  " + person;
            for(PersonUsable personUsable: personUsage.keySet()) {
                if(personUsable == null) {
                    report += "\n    waited for " + personUsage.get(personUsable) + " times.";
                }
                else {
                    report += "\n    used " + personUsable + " for " + personUsage.get(personUsable) + " times.";
                }
            }
        }
        return report + "\n";
    }
}
