package cz.cvut.fel.omo;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import cz.cvut.fel.omo.electronics.devices.DeviceFactory;
import cz.cvut.fel.omo.electronics.sensors.HumiditySensor;
import cz.cvut.fel.omo.electronics.sensors.LightSensor;
import cz.cvut.fel.omo.electronics.sensors.MotionSensor;
import cz.cvut.fel.omo.electronics.sensors.TemperatureSensor;
import cz.cvut.fel.omo.house.*;
import cz.cvut.fel.omo.inmates.people.*;
import cz.cvut.fel.omo.inmates.pets.Pet;
import cz.cvut.fel.omo.inmates.pets.PetType;
import cz.cvut.fel.omo.vehicles.Bike;
import cz.cvut.fel.omo.vehicles.Ski;

import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;

public class Main {


    public static void main(String[] args) {
        int house = 1;
        switch(house) {
            case 1:
                buildHouse1();
                break;
            case 2:
                buildHouse2();
                break;
        }
        Simulation simulation = new Simulation(50 , "input/config" + house + ".json", 20, 60, 20);
        try {
            FileWriter fileWriter1 = new FileWriter(new File("output/HouseConfigReport" + house + ".txt"));
            fileWriter1.write(simulation.houseConfigReport());
            fileWriter1.flush();
            simulation.run();
            fileWriter1 = new FileWriter(new File("output/ConsumptionReport" + house + ".txt"));
            fileWriter1.write(simulation.consumptionReport());
            fileWriter1.flush();
            fileWriter1 = new FileWriter(new File("output/EventReport" + house + ".txt"));
            fileWriter1.write(simulation.eventReport());
            fileWriter1.flush();
            fileWriter1 = new FileWriter(new File("output/UsageReport" + house + ".txt"));
            fileWriter1.write(simulation.usageReport());
            fileWriter1.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void buildHouse1() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        HouseBuilder houseBuilder = new HouseBuilder();
        DeviceFactory df = new DeviceFactory();
        Room livingRoom = new RoomBuilder(ROOMTYPE.Livingroom).
                addPet(new Pet(PetType.Dog)).
                addPerson(new Child()).
                addPerson(new Mom()).
                addDevice(df.makeLight(1, 50, 20)).
                addDevice(df.makeRadiator(5, 800, 18)).
                addDevice(df.makeRadiator(5, 800, 50)).
                addDevice(df.makeAC(5, 500, 72)).
                addDevice(df.makeTV(2, 200, 40)).
                addDevice(df.makeComputer(5, 800, 5)).
                addDevice(df.makeHumidifier(5, 800, 5)).
                addSensor(new HumiditySensor()).
                addSensor(new TemperatureSensor()).
                addSensor(new MotionSensor()).
                addSensor(new LightSensor()).
                setTemperature(16).
                setTargetTemperature(25).
                setHumidity(30).
                setTargetHumidity(70).
                getResult();
        Room kitchen = new RoomBuilder(ROOMTYPE.Kitchen).
                addPerson(new Dad()).
                addDevice(df.makeLight(1, 50, 20)).
                addDevice(df.makeLight(1, 50, 30)).
                addDevice(df.makeRadiator(5, 800, 18)).
                addDevice(df.makeFridge(1, 50, 200, 5)).
                addDevice(df.makeOven(1, 50, 1)).
                addSensor(new TemperatureSensor()).
                addSensor(new MotionSensor()).
                addSensor(new LightSensor()).
                setTemperature(18).
                setTargetTemperature(24).
                setHumidity(30).
                setTargetHumidity(65).
                getResult();
        Room bathroom = new RoomBuilder(ROOMTYPE.Bathroom).
                addPet(new Pet(PetType.Hamster)).
                addPet(new Pet(PetType.Hamster)).
                addDevice(df.makeLight(1, 50, 60)).
                addDevice(df.makeLight(1, 50, 80)).
                addDevice(df.makeRadiator(5, 800, 35)).
                addSensor(new TemperatureSensor()).
                addSensor(new MotionSensor()).
                setTemperature(18).
                setTargetTemperature(24).
                getResult();
        Room bedroom1 = new RoomBuilder(ROOMTYPE.Bedroom).
                addPerson(new Grandma()).
                addDevice(df.makeLight(1, 50, 90)).
                addDevice(df.makeLight(1, 50, 20)).
                addDevice(df.makeRadiator(5, 800, 27)).
                addDevice(df.makeComputer(5, 150, 37)).
                addDevice(df.makeComputer(5, 350, 12)).
                addDevice(df.makeHumidifier(5, 800, 5)).
                addSensor(new HumiditySensor()).
                addSensor(new TemperatureSensor()).
                addSensor(new MotionSensor()).
                addSensor(new LightSensor()).
                setTemperature(18).
                setTargetTemperature(24).
                setHumidity(30).
                setTargetHumidity(65).
                getResult();
        Room bedroom2 = new RoomBuilder(ROOMTYPE.Bedroom).
                addPerson(new Grandpa()).
                addPerson(new Child()).
                addPet(new Pet(PetType.Dog)).
                addDevice(df.makeLight(1, 50, 90)).
                addDevice(df.makeLight(1, 50, 20)).
                addDevice(df.makeRadiator(5, 800, 27)).
                addDevice(df.makeComputer(5, 150, 37)).
                addDevice(df.makeComputer(5, 350, 12)).
                addSensor(new TemperatureSensor()).
                addSensor(new MotionSensor()).
                addSensor(new LightSensor()).
                setTemperature(18).
                setTargetTemperature(24).
                setHumidity(30).
                setTargetHumidity(65).
                getResult();
        Garage garage = new Garage();
        garage.addVehicle(new Ski());
        garage.addVehicle(new Bike());
        garage.addVehicle(new Bike());
        House house1 = houseBuilder.
                addRoom(livingRoom).
                addRoom(bathroom).
                addRoom(kitchen).
                addRoom(bedroom1).
                addRoom(bedroom2).
                addRoom(garage).
                getResult();
        try {
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(
                    new FileOutputStream("input/config1.json"), house1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void buildHouse2() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
        HouseBuilder houseBuilder = new HouseBuilder();
        DeviceFactory df = new DeviceFactory();
        Room livingRoom = new RoomBuilder(ROOMTYPE.Livingroom).
                addPet(new Pet(PetType.Dog)).
                addPerson(new Child()).

                addDevice(df.makeRadiator(5, 800, 50)).
                addDevice(df.makeLight(1, 50, 30)).
                addDevice(df.makeRadiator(5, 800, 18)).
                addDevice(df.makeFridge(1, 50, 200, 5)).
                addDevice(df.makeOven(1, 50, 1)).
                addDevice(df.makeAC(5, 500, 72)).
                addSensor(new MotionSensor()).
                addSensor(new LightSensor()).
                setTemperature(16).
                setTargetTemperature(25).
                setHumidity(30).
                setTargetHumidity(70).
                getResult();
        Room kitchen = new RoomBuilder(ROOMTYPE.Kitchen).
                addPerson(new Dad()).
                addDevice(df.makeLight(1, 50, 20)).

                addDevice(df.makeTV(2, 200, 40)).
                addDevice(df.makeComputer(5, 800, 5)).
                addDevice(df.makeHumidifier(5, 800, 5)).
                addSensor(new HumiditySensor()).
                addSensor(new TemperatureSensor()).
                addSensor(new TemperatureSensor()).
                addDevice(df.makeHumidifier(5, 800, 5)).
                addDevice(df.makeRadiator(5, 800, 27)).
                addDevice(df.makeComputer(5, 150, 37)).
                addDevice(df.makeComputer(5, 350, 12)).
                addSensor(new TemperatureSensor()).
                addSensor(new HumiditySensor()).
                addSensor(new TemperatureSensor()).
                addSensor(new MotionSensor()).
                addSensor(new LightSensor()).
                setTemperature(18).
                setTargetTemperature(24).
                setHumidity(30).
                setTargetHumidity(65).
                getResult();
        Room bathroom = new RoomBuilder(ROOMTYPE.Bathroom).
                addDevice(df.makeLight(1, 50, 60)).
                addDevice(df.makeLight(1, 50, 80)).
                addDevice(df.makeRadiator(5, 800, 35)).
                addSensor(new TemperatureSensor()).
                addSensor(new MotionSensor()).
                setTemperature(18).
                setTargetTemperature(24).
                getResult();
        Room bedroom1 = new RoomBuilder(ROOMTYPE.Bedroom).
                addPerson(new Grandma()).
                addDevice(df.makeLight(1, 50, 90)).
                addDevice(df.makeLight(1, 50, 20)).
                addDevice(df.makeRadiator(5, 800, 27)).
                addSensor(new MotionSensor()).
                addSensor(new LightSensor()).
                addDevice(df.makeComputer(5, 150, 37)).
                addDevice(df.makeComputer(5, 350, 12)).
                setTemperature(18).
                setTargetTemperature(24).
                setHumidity(30).
                setTargetHumidity(65).
                getResult();
        Room bedroom2 = new RoomBuilder(ROOMTYPE.Bedroom).
                addPerson(new Grandpa()).
                addPerson(new Child()).
                addPerson(new Mom()).
                addDevice(df.makeLight(1, 50, 20)).
                addDevice(df.makeRadiator(5, 800, 18)).
                addDevice(df.makeLight(1, 50, 90)).
                addDevice(df.makeLight(1, 50, 20)).
                addSensor(new MotionSensor()).
                addSensor(new LightSensor()).
                setTemperature(18).
                setTargetTemperature(24).
                setHumidity(30).
                setTargetHumidity(65).
                getResult();
        Garage garage = new Garage();
        garage.addVehicle(new Ski());
        garage.addVehicle(new Bike());
        garage.addVehicle(new Bike());
        garage.addDevice(df.makeRadiator(2,300,20));
        garage.addSensor(new TemperatureSensor());
        House house1 = houseBuilder.
                addRoom(livingRoom).
                addRoom(bathroom).
                addRoom(kitchen).
                addRoom(bedroom1).
                addRoom(bedroom2).
                addRoom(garage).
                getResult();
        try {
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(
                    new FileOutputStream("input/config2.json"), house1);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}

