package cz.cvut.fel.omo.electronics.sensors;

import cz.cvut.fel.omo.electronics.events.SensorEvent;
import cz.cvut.fel.omo.house.Room;

public class TemperatureSensor extends Sensor {
    @Override
    public void report() {
        Room room = eventHandler.getRoom();
        int temperature = room.getTemperature();
        SensorEvent event = new SensorEvent(temperature, this);
        eventHandler.handle(event);
    }
}
