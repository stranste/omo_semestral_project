package cz.cvut.fel.omo.electronics.devices;

import cz.cvut.fel.omo.electronics.devices.state.BrokenState;
import cz.cvut.fel.omo.electronics.devices.state.TurnOnState;
import cz.cvut.fel.omo.electronics.events.BrokenDeviceEvent;

public class Radiator extends Device {

    public Radiator(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        super(idleElectricityConsumption, activeElectricityConsumption, durability);
    }

    public Radiator() {
        super(5,10,10);
    }

    @Override
    public void update() {
        super.update();
        if(state instanceof TurnOnState) {
            int temp = eventHandler.getRoom().getTemperature();
            eventHandler.getRoom().setTemperature(temp + 3);
        }
        else if(state instanceof BrokenState) {
            eventHandler.handle(new BrokenDeviceEvent(this));
        }
    }
}
