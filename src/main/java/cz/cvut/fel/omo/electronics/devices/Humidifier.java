package cz.cvut.fel.omo.electronics.devices;

import cz.cvut.fel.omo.electronics.devices.state.BrokenState;
import cz.cvut.fel.omo.electronics.devices.state.TurnOnState;
import cz.cvut.fel.omo.electronics.events.BrokenDeviceEvent;

public class Humidifier extends Device {

    public Humidifier(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        super(idleElectricityConsumption, activeElectricityConsumption, durability);
    }

    public Humidifier() {
        super(5,10,10);
    }

    @Override
    public void update() {
        super.update();
        if(state instanceof TurnOnState) {
            int hum = eventHandler.getRoom().getHumidity();
            if(hum + 5 <= 100) {
                eventHandler.getRoom().setHumidity(hum + 5);
            }
            else {
                eventHandler.getRoom().setHumidity(100);
            }
        }
        else if(state instanceof BrokenState) {
            eventHandler.handle(new BrokenDeviceEvent(this));
        }
    }
}
