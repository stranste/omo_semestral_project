package cz.cvut.fel.omo.electronics.devices.state;

import cz.cvut.fel.omo.electronics.devices.Device;

public class TurnOnState implements DeviceState {
    @Override
    public int getConsumption(Device device) {
        return device.getActiveElectricityConsumption();
    }

    @Override
    public void turnOn(Device device) {

    }

    @Override
    public void turnOff(Device device) {
        device.setState(new TurnOffState());
    }

    @Override
    public String toString() {
        return "turned on";
    }
}
