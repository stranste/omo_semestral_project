package cz.cvut.fel.omo.electronics.devices.state;

import cz.cvut.fel.omo.electronics.devices.Device;

public class TurnOffState implements DeviceState {

    @Override
    public int getConsumption(Device device) {
        return device.getIdleElectricityConsumption();
    }

    @Override
    public void turnOn(Device device) {
        device.setState(new TurnOnState());
    }

    @Override
    public void turnOff(Device device) {

    }

    @Override
    public String toString() {
        return "turned off";
    }
}
