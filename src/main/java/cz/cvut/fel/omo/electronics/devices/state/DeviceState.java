package cz.cvut.fel.omo.electronics.devices.state;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import cz.cvut.fel.omo.electronics.devices.*;

@JsonTypeInfo( use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = BrokenState.class, name = "broken-state"),
        @JsonSubTypes.Type(value = TurnOnState.class, name = "turn-on-state"),
        @JsonSubTypes.Type(value = TurnOffState.class, name = "turn-off-state"),
})
public interface DeviceState {
    int getConsumption(Device device);
    void turnOn(Device device);
    void turnOff(Device device);
}
