package cz.cvut.fel.omo.electronics.sensors;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import cz.cvut.fel.omo.house.EventHandler;
import cz.cvut.fel.omo.house.EventHandlerDeserializer;

@JsonTypeInfo( use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = HumiditySensor.class, name = "humidity-sensor"),
        @JsonSubTypes.Type(value = LightSensor.class, name = "light-sensor"),
        @JsonSubTypes.Type(value = MotionSensor.class, name = "motion-sensor"),
        @JsonSubTypes.Type(value = TemperatureSensor.class, name = "temperature-sensor")
})
public abstract class Sensor {
    @JsonDeserialize(using = EventHandlerDeserializer.class)
    protected EventHandler eventHandler;

    public void setEventHandler(EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    public EventHandler getEventHandler() {
        return eventHandler;
    }

    public abstract void report();

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " (Serial:" + String.format("%04d",this.hashCode() % 10000) + ")";
    }
}
