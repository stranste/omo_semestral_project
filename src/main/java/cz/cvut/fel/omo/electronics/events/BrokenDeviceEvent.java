package cz.cvut.fel.omo.electronics.events;

import cz.cvut.fel.omo.electronics.devices.Device;

public class BrokenDeviceEvent {
    private Device device;

    public BrokenDeviceEvent(Device device) {
        this.device = device;
    }

    public Device getDevice() {
        return device;
    }

    @Override
    public String toString() {
        return "BrokenDevice event from device: " + device + ".";
    }
}
