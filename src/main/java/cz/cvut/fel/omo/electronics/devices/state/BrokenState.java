package cz.cvut.fel.omo.electronics.devices.state;

import cz.cvut.fel.omo.electronics.devices.Device;

public class BrokenState implements DeviceState {
    @Override
    public int getConsumption(Device device) {
        return 0;
    }

    @Override
    public void turnOn(Device device) {

    }

    @Override
    public void turnOff(Device device) {

    }

    @Override
    public String toString() {
        return "broken";
    }
}
