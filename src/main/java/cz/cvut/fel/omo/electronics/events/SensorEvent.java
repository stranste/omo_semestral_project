package cz.cvut.fel.omo.electronics.events;

import cz.cvut.fel.omo.electronics.sensors.Sensor;

public class SensorEvent {
    private int value;
    private Sensor sensor;

    public SensorEvent(int value, Sensor sensor) {
        this.value = value;
        this.sensor = sensor;
    }

    public int getValue() {
        return value;
    }

    public Sensor getSensor() {
        return sensor;
    }

    @Override
    public String toString() {
        return "Sensor event from device: " + sensor + " with value: " + value + ".";
    }
}
