package cz.cvut.fel.omo.electronics.devices;

public class DeviceFactory {
    public Light makeLight(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        return new Light(idleElectricityConsumption, activeElectricityConsumption, durability);
    }
    public Radiator makeRadiator(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        return new Radiator(idleElectricityConsumption, activeElectricityConsumption, durability);
    }
    public AirConditioner makeAC(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        return new AirConditioner(idleElectricityConsumption, activeElectricityConsumption, durability);
    }
    public Humidifier makeHumidifier(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        return new Humidifier(idleElectricityConsumption, activeElectricityConsumption, durability);
    }
    public Computer makeComputer(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        return new Computer(idleElectricityConsumption, activeElectricityConsumption, durability);
    }
    public Fridge makeFridge(int idleElectricityConsumption, int activeElectricityConsumption, int durability, int supplies) {
        return new Fridge(idleElectricityConsumption, activeElectricityConsumption, durability, supplies);
    }
    public Oven makeOven(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        return new Oven(idleElectricityConsumption, activeElectricityConsumption, durability);
    }
    public TV makeTV(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        return new TV(idleElectricityConsumption, activeElectricityConsumption, durability);
    }
}
