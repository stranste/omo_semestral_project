package cz.cvut.fel.omo.electronics.devices;

import cz.cvut.fel.omo.api.PersonUsable;
import cz.cvut.fel.omo.electronics.devices.state.BrokenState;
import cz.cvut.fel.omo.electronics.events.BrokenDeviceEvent;
import cz.cvut.fel.omo.electronics.events.FridgeEmptyEvent;

public class Fridge extends Device implements PersonUsable {
    private int supplies;
    private final int maxSupplies;
    public Fridge(int idleElectricityConsumption, int activeElectricityConsumption, int durability, int supplies) {
        super(idleElectricityConsumption, activeElectricityConsumption, durability);
        this.maxSupplies = this.supplies = supplies;
    }

    public Fridge() {
        super(5,10,10);
        this.maxSupplies = this.supplies = 20;
    }

    @Override
    public boolean use() {
        if(!(state instanceof BrokenState)  && !inUse) {
            inUse = true;
            return --supplies > 0;
        }
        return false;
    }

    @Override
    public void update() {
        super.update();

        if(state instanceof BrokenState) {
            eventHandler.handle(new BrokenDeviceEvent(this));
        }
        if(supplies < 0) {
            eventHandler.handle(new FridgeEmptyEvent(this));
        }
    }

    public int getMaxSupplies() {
        return maxSupplies;
    }

    public int getSupplies() {
        return supplies;
    }

    public void refillSupplies() {
        supplies = maxSupplies;
    }
}
