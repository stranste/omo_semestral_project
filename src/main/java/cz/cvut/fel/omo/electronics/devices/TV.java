package cz.cvut.fel.omo.electronics.devices;

import cz.cvut.fel.omo.api.PersonUsable;
import cz.cvut.fel.omo.electronics.devices.state.BrokenState;
import cz.cvut.fel.omo.electronics.events.BrokenDeviceEvent;

public class TV extends Device implements PersonUsable {

    public TV(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        super(idleElectricityConsumption, activeElectricityConsumption, durability);
    }

    public TV() {
        super(5,10,10);
    }

    @Override
    public boolean use() {
        if(!(state instanceof BrokenState)  && !inUse) {
            inUse = true;
            return true;
        }
        return false;
    }

    @Override
    public void update() {
        super.update();
        if(state instanceof BrokenState) {
            eventHandler.handle(new BrokenDeviceEvent(this));
        }
    }
}
