package cz.cvut.fel.omo.electronics.devices;

import cz.cvut.fel.omo.electronics.devices.state.BrokenState;
import cz.cvut.fel.omo.electronics.devices.state.TurnOnState;
import cz.cvut.fel.omo.electronics.events.BrokenDeviceEvent;

public class AirConditioner extends Device {

    public AirConditioner(int idle, int active, int durability) {
        super(idle, active, durability);
    }

    public AirConditioner() {
        super(5,10,10);
    }

    @Override
    public void update() {
        super.update();
        if(state instanceof TurnOnState) {
            int temp = eventHandler.getRoom().getTemperature();
            eventHandler.getRoom().setTemperature(temp - 1);
        }
        else if(state instanceof BrokenState) {
            eventHandler.handle(new BrokenDeviceEvent(this));
        }
    }
}
