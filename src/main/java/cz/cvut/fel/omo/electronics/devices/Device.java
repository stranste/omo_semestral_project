package cz.cvut.fel.omo.electronics.devices;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import cz.cvut.fel.omo.api.ElectricityConsuming;
import cz.cvut.fel.omo.electronics.devices.state.BrokenState;
import cz.cvut.fel.omo.electronics.devices.state.DeviceState;
import cz.cvut.fel.omo.electronics.devices.state.TurnOffState;
import cz.cvut.fel.omo.electronics.devices.state.TurnOnState;
import cz.cvut.fel.omo.house.EventHandler;
import cz.cvut.fel.omo.house.EventHandlerDeserializer;

import java.io.*;
import java.util.Random;

@JsonTypeInfo( use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = AirConditioner.class, name = "air-conditioner"),
        @JsonSubTypes.Type(value = Light.class, name = "light"),
        @JsonSubTypes.Type(value = Radiator.class, name = "radiator"),
        @JsonSubTypes.Type(value = Humidifier.class, name = "humidifier"),
        @JsonSubTypes.Type(value = Fridge.class, name = "fridge"),
        @JsonSubTypes.Type(value = Computer.class, name = "computer"),
        @JsonSubTypes.Type(value = TV.class, name = "tv"),
        @JsonSubTypes.Type(value = Oven.class, name = "oven")
})
public abstract class Device implements ElectricityConsuming {
    private String manual;
    protected int idleElectricityConsumption;
    protected int activeElectricityConsumption;
    protected int durability;
    protected final int maxDurability;
    protected DeviceState state;
    protected boolean inUse = false;

    public DeviceState getState() {
        return state;
    }

    public Device(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        this.idleElectricityConsumption = idleElectricityConsumption;
        this.activeElectricityConsumption = activeElectricityConsumption;
        this.maxDurability = this.durability = durability;
        state = new TurnOffState();
    }

    @JsonDeserialize(using = EventHandlerDeserializer.class)
    protected EventHandler eventHandler;

    public EventHandler getEventHandler() {
        return eventHandler;
    }

    public int getDurability() {
        return durability;
    }

    public int getIdleElectricityConsumption() {
        return idleElectricityConsumption;
    }

    public int getActiveElectricityConsumption() {
        return activeElectricityConsumption;
    }

    public int getMaxDurability() {
        return maxDurability;
    }

    public boolean isInUse() {
        return inUse;
    }

    public void setEventHandler(EventHandler eventHandler) {
        this.eventHandler = eventHandler;
    }

    public void setState(DeviceState state) {
        this.state = state;
    }

    public void turnOn() {
        state.turnOn(this);
    }

    public void turnOff() {
        state.turnOff(this);
        inUse = false;
    }

    public int getConsumption() {
        return state.getConsumption(this);
    }

    public void update() {
        Random random = new Random();
        if(state instanceof TurnOnState) {
            durability -= random.nextBoolean() ? 1 : 0;
            if(durability < 0) {
                state = new BrokenState();
            }
        }
    }

    public void destroy() {
        Random random = new Random();
        durability -= random.nextInt(20);
    }

    @JsonIgnore
    public String getManual() {
        if(manual == null) {
            try {
                BufferedReader br = new BufferedReader(new FileReader(new File("input/manual")));
                manual = br.readLine();
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return manual;
    }

    public void repair(String manual) {
        if(manual.equals(this.manual)) {
            durability = maxDurability;
            state = new TurnOffState();
        }
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " (Serial:" + String.format("%04d",this.hashCode() % 10000) + ")";
    }
}
