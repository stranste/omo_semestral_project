package cz.cvut.fel.omo.electronics.sensors;

import cz.cvut.fel.omo.electronics.events.SensorEvent;
import cz.cvut.fel.omo.house.Room;

public class LightSensor extends Sensor {
    @Override
    public void report() {
        Room room = eventHandler.getRoom();
        int light = room.getNaturalLight();
        SensorEvent event = new SensorEvent(light, this);
        eventHandler.handle(event);
    }
}
