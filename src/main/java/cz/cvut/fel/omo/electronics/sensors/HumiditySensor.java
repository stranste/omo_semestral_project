package cz.cvut.fel.omo.electronics.sensors;

import cz.cvut.fel.omo.electronics.events.SensorEvent;
import cz.cvut.fel.omo.house.Room;

public class HumiditySensor extends Sensor {
    @Override
    public void report() {
        Room room = eventHandler.getRoom();
        int humidity = room.getHumidity();
        SensorEvent event = new SensorEvent(humidity, this);
        eventHandler.handle(event);
    }
}
