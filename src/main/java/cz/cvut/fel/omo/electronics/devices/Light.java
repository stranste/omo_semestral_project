package cz.cvut.fel.omo.electronics.devices;

import cz.cvut.fel.omo.electronics.devices.state.BrokenState;
import cz.cvut.fel.omo.electronics.events.BrokenDeviceEvent;

public class Light extends Device {

    public Light(int idleElectricityConsumption, int activeElectricityConsumption, int durability) {
        super(idleElectricityConsumption, activeElectricityConsumption, durability);
    }

    public Light() {
        super(5, 10, 10);
    }

    @Override
    public void update() {
        super.update();
        if(state instanceof BrokenState) {
            eventHandler.handle(new BrokenDeviceEvent(this));
        }
    }
}
