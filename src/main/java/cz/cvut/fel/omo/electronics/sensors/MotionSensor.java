package cz.cvut.fel.omo.electronics.sensors;

import cz.cvut.fel.omo.electronics.events.SensorEvent;
import cz.cvut.fel.omo.house.Room;

public class MotionSensor extends Sensor {
    @Override
    public void report() {
        Room room = eventHandler.getRoom();
        boolean isEmpty = room.getPersons().isEmpty() && room.getPets().isEmpty();
        SensorEvent event = new SensorEvent(isEmpty ? 0 : 1, this);
        eventHandler.handle(event);
    }
}
