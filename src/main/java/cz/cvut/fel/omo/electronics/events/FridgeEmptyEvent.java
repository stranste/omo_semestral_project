package cz.cvut.fel.omo.electronics.events;

import cz.cvut.fel.omo.electronics.devices.Fridge;

public class FridgeEmptyEvent {
    private Fridge fridge;

    public FridgeEmptyEvent(Fridge fridge) {
        this.fridge = fridge;
    }

    public Fridge getFridge() {
        return fridge;
    }

    @Override
    public String toString() {
        return "EmptyFridge event from device: " + fridge + ".";
    }
}
