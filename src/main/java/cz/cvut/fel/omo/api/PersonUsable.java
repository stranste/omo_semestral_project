package cz.cvut.fel.omo.api;

public interface PersonUsable {

    boolean use();

    void turnOn();

    void turnOff();
}
