package cz.cvut.fel.omo.api;

import com.fasterxml.jackson.annotation.JsonIgnore;

public interface ElectricityConsuming {
    @JsonIgnore
    int getConsumption();
}
