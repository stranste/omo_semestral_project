package cz.cvut.fel.omo.vehicles;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import cz.cvut.fel.omo.api.PersonUsable;

@JsonTypeInfo( use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Ski.class, name = "ski"),
        @JsonSubTypes.Type(value = Bike.class, name = "bike"),
})
public abstract class Vehicle implements PersonUsable {
    protected boolean inUse = false;

    public Vehicle() {

    }

    public boolean use() {
        if(!inUse) {
            inUse = true;
            return true;
        }
        return false;
    }

    public void turnOff() {
        inUse = false;
    }

    public void turnOn() {

    }

    public boolean isInUse() {
        return inUse;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName()  + " (Model: " + String.format("%03d",this.hashCode() % 100) + ")";
    }
}
