package cz.cvut.fel.omo.inmates.pets;

public enum PetType {
    Hamster,
    Cat,
    Dog
}
