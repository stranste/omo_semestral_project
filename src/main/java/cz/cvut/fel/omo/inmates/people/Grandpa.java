package cz.cvut.fel.omo.inmates.people;

import cz.cvut.fel.omo.electronics.events.BrokenDeviceEvent;
import cz.cvut.fel.omo.house.Room;

import java.util.*;

public class Grandpa extends Person {

    private Queue<BrokenDeviceEvent> brokenDeviceEvents;
    public Grandpa() {
        super();
        brokenDeviceEvents = new ArrayDeque<BrokenDeviceEvent>();
    }

    public void addBrokenDeviceEvent(BrokenDeviceEvent event) {
        brokenDeviceEvents.add(event);
    }

    @Override
    public void update(List<Room> rooms) {
        List<Room> newRooms = new ArrayList<Room>(rooms);
        Collections.shuffle(newRooms);

        currentRoom = findMyRoom(rooms);

        if(deviceInUse == null) {
            if(brokenDeviceEvents.size() > 0) {
                BrokenDeviceEvent event = brokenDeviceEvents.poll();
                String manual = event.getDevice().getManual();
                event.getDevice().repair(manual);
                return;
            }
            goTo(newRooms.get(0));
            if(random.nextBoolean()) {
                findNewDevice(currentRoom);
            }
            else {
                findNewVehicle(newRooms);
            }
        }
        else {
            if(useTime <= 0) { //device already saturated useTime
                deviceInUse.turnOff();
                deviceInUse = null;
            }
            else if(deviceInUse.use()) { //using the device
                useTime--;
            }
            else { //device broke down
                deviceInUse.turnOff();
                deviceInUse = null;
            }
        }
    }
}
