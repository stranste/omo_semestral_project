package cz.cvut.fel.omo.inmates.people;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import cz.cvut.fel.omo.api.PersonUsable;
import cz.cvut.fel.omo.electronics.devices.Device;
import cz.cvut.fel.omo.house.Garage;
import cz.cvut.fel.omo.house.Room;
import cz.cvut.fel.omo.vehicles.Vehicle;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@JsonTypeInfo( use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({
        @JsonSubTypes.Type(value = Dad.class, name = "dad"),
        @JsonSubTypes.Type(value = Mom.class, name = "mom"),
        @JsonSubTypes.Type(value = Grandma.class, name = "grandma"),
        @JsonSubTypes.Type(value = Grandpa.class, name = "grandpa"),
        @JsonSubTypes.Type(value = Child.class, name = "child"),

})
public abstract class Person {

    protected Random random;
    protected PersonUsable deviceInUse;
    protected int useTime;
    protected Room currentRoom;

    public Person() {
        random = new Random();
    }

    public PersonUsable getDeviceInUse() {
        return deviceInUse;
    }

    public abstract void update(List<Room> rooms);

    protected Room findMyRoom(List<Room> rooms) {
        Room myRoom = rooms.get(0);
        for(Room room:rooms) {
            if(room.getPersons().contains(this)){
                myRoom = room;
            }
        }
        return myRoom;
    }

    protected void findNewDevice(Room currentRoom) {
        Random random = new Random();
        List<Device> devices = new ArrayList<Device>(currentRoom.getDevices());
        Collections.shuffle(devices);
        for(int i = 0; i < devices.size(); i++) {
            if(devices.get(i) instanceof PersonUsable device) {
                if(device.use()) {
                    device.turnOn();
                    deviceInUse = device;
                    useTime = random.nextInt(5);
                    break;
                }
            }
        }
    }

    protected void findNewVehicle(List<Room> newRooms) {
        for (Room room:newRooms) {
            if(room instanceof Garage garage) {
                for (Vehicle vehicle:garage.getVehicles()) {
                    if(vehicle.use()) {
                        vehicle.turnOn();
                        deviceInUse = vehicle;
                        useTime = random.nextInt(5);
                        return;
                    }
                }
            }
        }
    }

    protected void goTo(Room to) {
        currentRoom.removePerson(this);
        to.addPerson(this);
        currentRoom = to;
    }

    @Override
    public String toString() {
        return this.getClass().getSimpleName() + " (SSN: " + String.format("%06d",this.hashCode() % 1000000) + ")";
    }
}
