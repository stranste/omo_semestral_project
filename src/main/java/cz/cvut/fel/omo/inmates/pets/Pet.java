package cz.cvut.fel.omo.inmates.pets;

import cz.cvut.fel.omo.electronics.devices.Device;
import cz.cvut.fel.omo.house.Room;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class Pet {
    private final PetType type;
    private final int size;

    public Pet(PetType type) {
        this.type = type;
        switch (type) {
            case Cat -> size = 10;
            case Hamster -> size = 5;
            default -> size = 20;
        }
    }

    public Pet() {
        this.type = PetType.Dog;
        size = 20;
    }

    public int getSize() {
        return size;
    }

    public PetType getType() {
        return type;
    }

    public void update(List<Room> rooms) {
        List<Room> newRooms = new ArrayList<Room>(rooms);
        Room myRoom = rooms.get(0);
        for(Room room:rooms) {
            if(room.getPets().contains(this)){
                myRoom = room;
            }
        }
        Collections.shuffle(newRooms);
        Random random = new Random();

        if(random.nextInt(50) < size) {
            List<Device> devices = new ArrayList<Device>(myRoom.getDevices());
            Collections.shuffle(devices);
            if(devices.size() > 0) devices.get(0).destroy();
        }
        goTo(newRooms.get(0), myRoom);
    }

    private void goTo(Room to, Room from) {
        from.removePet(this);
        to.addPet(this);
    }

    @Override
    public String toString() {
        return type.toString();
    }
}
