package cz.cvut.fel.omo.inmates.people;

import cz.cvut.fel.omo.house.Room;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Child extends Person {
    public Child() {
        super();
    }

    @Override
    public void update(List<Room> rooms) {
        List<Room> newRooms = new ArrayList<Room>(rooms);
        Collections.shuffle(newRooms);

        currentRoom = findMyRoom(rooms);

        if(deviceInUse == null) {
            goTo(newRooms.get(0));
            if(random.nextBoolean()) {
                findNewDevice(currentRoom);
            }
            else {
                findNewVehicle(newRooms);
            }
        }
        else {
            if(useTime <= 0) { //device already saturated useTime
                deviceInUse.turnOff();
                deviceInUse = null;
            }
            else if(deviceInUse.use()) { //using the device
                useTime--;
            }
            else { //device broke down
                deviceInUse.turnOff();
                deviceInUse = null;
            }
        }
    }
}
