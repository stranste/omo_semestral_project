package cz.cvut.fel.omo.inmates.people;

import cz.cvut.fel.omo.electronics.events.FridgeEmptyEvent;
import cz.cvut.fel.omo.house.Room;

import java.util.*;

public class Mom extends Person {

    private Queue<FridgeEmptyEvent> fridgeEmptyEvents;
    public Mom() {
        super();
        fridgeEmptyEvents = new ArrayDeque<FridgeEmptyEvent>();
    }

    public void addFridgeEvent(FridgeEmptyEvent event) {
        fridgeEmptyEvents.add(event);
    }

    @Override
    public void update(List<Room> rooms) {
        List<Room> newRooms = new ArrayList<Room>(rooms);
        Collections.shuffle(newRooms);

        currentRoom = findMyRoom(rooms);

        if(deviceInUse == null) {
            if(fridgeEmptyEvents.size() > 0) {
                FridgeEmptyEvent event = fridgeEmptyEvents.poll();
                event.getFridge().refillSupplies();
                return;
            }
            goTo(newRooms.get(0));
            if(random.nextBoolean()) {
                findNewDevice(currentRoom);
            }
            else {
                findNewVehicle(newRooms);
            }
        }
        else {
            if(useTime <= 0) { //device already saturated useTime
                deviceInUse.turnOff();
                deviceInUse = null;
            }
            else if(deviceInUse.use()) { //using the device
                useTime--;
            }
            else { //device broke down
                deviceInUse.turnOff();
                deviceInUse = null;
            }
        }
    }
}
