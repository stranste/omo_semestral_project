package cz.cvut.fel.omo.house;

import java.util.ArrayList;
import java.util.List;

public class House {
    private List<Room> rooms;

    public House() {
        rooms = new ArrayList<Room>();
    }

    public List<Room> getRooms() {
        return rooms;
    }

    public void addRoom(Room room) {
        rooms.add(room);
    }
}
