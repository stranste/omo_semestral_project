package cz.cvut.fel.omo.house;

public class HouseBuilder {
    private House house = new House();

    public HouseBuilder addRoom(Room room) {
        house.addRoom(room);
        return this;
    }

    public House getResult() {
        return house;
    }
}
