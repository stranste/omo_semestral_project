package cz.cvut.fel.omo.house;

public enum ROOMTYPE {
    Bathroom,
    Bedroom,
    Livingroom,
    Kitchen,
    Garage
}