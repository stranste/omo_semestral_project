package cz.cvut.fel.omo.house;

import cz.cvut.fel.omo.vehicles.Vehicle;

import java.util.ArrayList;
import java.util.List;

public class Garage extends Room {
    private List<Vehicle> vehicles;

    public Garage() {
        super(ROOMTYPE.Garage);
        vehicles = new ArrayList<Vehicle>();
    }

    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    public void addVehicle(Vehicle vehicle) {
        vehicles.add(vehicle);
    }
}
