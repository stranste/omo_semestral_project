package cz.cvut.fel.omo.house;

import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonStreamContext;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.IntNode;

import java.io.IOException;

public class EventHandlerDeserializer extends StdDeserializer<EventHandler> {

    public EventHandlerDeserializer() {
        this(null);
    }

    public EventHandlerDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public EventHandler deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException, JacksonException {
        JsonStreamContext parent = jsonParser.getParsingContext().getParent();
        while(!(parent.getCurrentValue() instanceof Room)) {
            parent = parent.getParent();
        }
        JsonNode node = jsonParser.getCodec().readTree(jsonParser);
        int id = (Integer) ((IntNode) node.get("room")).numberValue();
        EventHandler eventHandler = ((Room) parent.getCurrentValue()).getEventHandler();
        if(eventHandler == null) {
            eventHandler = new EventHandler((Room) parent.getCurrentValue());
        }
        return eventHandler;
    }
}