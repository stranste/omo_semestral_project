package cz.cvut.fel.omo.house;

import com.fasterxml.jackson.annotation.JsonIgnore;
import cz.cvut.fel.omo.electronics.devices.*;
import cz.cvut.fel.omo.electronics.events.BrokenDeviceEvent;
import cz.cvut.fel.omo.electronics.events.FridgeEmptyEvent;
import cz.cvut.fel.omo.electronics.events.SensorEvent;
import cz.cvut.fel.omo.electronics.sensors.HumiditySensor;
import cz.cvut.fel.omo.electronics.sensors.LightSensor;
import cz.cvut.fel.omo.electronics.sensors.MotionSensor;
import cz.cvut.fel.omo.electronics.sensors.TemperatureSensor;
import cz.cvut.fel.omo.inmates.people.*;

import java.util.ArrayList;
import java.util.List;

public class EventHandler {

    private Room room;
    private List<SensorEvent> sensorEvents;
    private List<BrokenDeviceEvent> brokenDeviceEvents;
    private List<FridgeEmptyEvent> fridgeEmptyEvents;

    public EventHandler(Room room) {
        this.room = room;
        sensorEvents = new ArrayList<SensorEvent>();
        brokenDeviceEvents = new ArrayList<BrokenDeviceEvent>();
        fridgeEmptyEvents = new ArrayList<FridgeEmptyEvent>();
    }

    public void handle(SensorEvent event) {
        sensorEvents.add(event);
    }

    public void handle(BrokenDeviceEvent event) {
        brokenDeviceEvents.add(event);
    }

    public void handle(FridgeEmptyEvent event) {
        fridgeEmptyEvents.add(event);
    }

    private void handleHumidity(SensorEvent event) {
        if(event.getValue() < room.getTargetHumidity()) {
            for (Device device:room.getDevices()) {
                if(device instanceof Humidifier) {
                    device.turnOn();
                }
            }
        }
        else {
            for (Device device:room.getDevices()) {
                if(device instanceof Humidifier) {
                    device.turnOff();
                }
            }
        }
    }

    private void handleTemperature(SensorEvent event) {
        if(event.getValue() < room.getTargetTemperature()) {
            for (Device device:room.getDevices()) {
                if(device instanceof Radiator) {
                    device.turnOn();
                }
                if(device instanceof AirConditioner) {
                    device.turnOff();
                }
            }
        }
        else {
            for (Device device:room.getDevices()) {
                if(device instanceof Radiator) {
                    device.turnOff();
                }
                if(device instanceof AirConditioner) {
                    device.turnOn();
                }
            }
        }
    }

    private void handleMotion(SensorEvent event) {
        if(event.getValue() != 0) {
            int naturalLightLevel = 0;
            for (SensorEvent event1:sensorEvents) {
                if(event1.getSensor() instanceof LightSensor) {
                    naturalLightLevel = event1.getValue();
                }
            }
            if(naturalLightLevel < 60) {
                for (Device device:room.getDevices()) {
                    if(device instanceof Light) {
                        device.turnOn();
                    }
                }
            }
            else {
                for (Device device:room.getDevices()) {
                    if(device instanceof Light) {
                        device.turnOff();
                    }
                }
            }
        }
        else
        {
            for (Device device:room.getDevices()) {
                if(device instanceof Light) {
                    device.turnOff();
                }
            }
        }
    }

    public void handleEvents() {
        for (SensorEvent event:sensorEvents) {
            if(event.getSensor() instanceof HumiditySensor) {
                handleHumidity(event);
            }
            if(event.getSensor() instanceof TemperatureSensor) {
                handleTemperature(event);
            }
            if(event.getSensor() instanceof MotionSensor) {
                handleMotion(event);
            }
        }
        for (FridgeEmptyEvent event:fridgeEmptyEvents) {
            for (Person person:room.getPersons()) {
                if(person instanceof Grandma) {
                    ((Grandma)person).addFridgeEvent(event);
                    break;
                }
                if(person instanceof Mom) {
                    ((Mom)person).addFridgeEvent(event);
                    break;
                }
            }
        }
        for (BrokenDeviceEvent event:brokenDeviceEvents) {
            for (Person person:room.getPersons()) {
                if(person instanceof Grandpa) {
                    ((Grandpa)person).addBrokenDeviceEvent(event);
                    break;
                }
                if(person instanceof Dad) {
                    ((Dad)person).addBrokenDeviceEvent(event);
                    break;
                }
            }
        }
    }

    public Room getRoom() {
        return room;
    }

    @JsonIgnore
    public List<Object> getAllEvents() {
        List<Object> retVal = new ArrayList<>();
        retVal.addAll(sensorEvents);
        retVal.addAll(brokenDeviceEvents);
        retVal.addAll(fridgeEmptyEvents);
        sensorEvents.clear();
        brokenDeviceEvents.clear();
        fridgeEmptyEvents.clear();
        return retVal;
    }
}
