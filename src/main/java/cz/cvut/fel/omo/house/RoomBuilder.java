package cz.cvut.fel.omo.house;

import cz.cvut.fel.omo.electronics.devices.Device;
import cz.cvut.fel.omo.electronics.sensors.Sensor;
import cz.cvut.fel.omo.inmates.people.Person;
import cz.cvut.fel.omo.inmates.pets.Pet;

public class RoomBuilder {

    private Room room;

    public RoomBuilder(ROOMTYPE type) {
        room = new Room(type);
    }

    public RoomBuilder setTemperature(int temperature) {
        room.setTemperature(temperature);
        return this;
    }

    public RoomBuilder setHumidity(int humidity) {
        room.setHumidity(humidity);
        return this;
    }

    public RoomBuilder setTargetTemperature(int temperature) {
        room.setTargetTemperature(temperature);
        return this;
    }

    public RoomBuilder setTargetHumidity(int humidity) {
        room.setTargetHumidity(humidity);
        return this;
    }

    public RoomBuilder addPerson(Person person) {
        room.addPerson(person);
        return this;
    }

    public RoomBuilder addPet(Pet pet) {
        room.addPet(pet);
        return this;
    }

    public RoomBuilder addSensor(Sensor sensor) {
        room.addSensor(sensor);

        return this;
    }

    public RoomBuilder addDevice(Device device) {
        room.addDevice(device);
        return this;
    }

    public Room getResult() {
        return room;
    }
}
