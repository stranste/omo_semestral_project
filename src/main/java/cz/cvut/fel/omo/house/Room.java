package cz.cvut.fel.omo.house;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import cz.cvut.fel.omo.electronics.devices.Device;
import cz.cvut.fel.omo.electronics.sensors.Sensor;
import cz.cvut.fel.omo.inmates.people.Person;
import cz.cvut.fel.omo.inmates.pets.Pet;

import java.util.ArrayList;
import java.util.List;

@JsonIdentityInfo(generator= ObjectIdGenerators.IntSequenceGenerator.class, property="roomid")
@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type")
@JsonSubTypes({ @JsonSubTypes.Type(value = Garage.class, name = "garage") })
public class Room {
    protected final ROOMTYPE roomtype;
    private List<Person> persons;

    private List<Pet> pets;

    private List<Sensor> sensors;
    private List<Device> devices;
    private int temperature = 20;

    private int humidity = 20;
    private int naturalLight = 0;
    private int targetTemperature = 20;
    private int targetHumidity = 20;
    @JsonDeserialize(using = EventHandlerDeserializer.class)
    private EventHandler eventHandler;

    public Room(ROOMTYPE roomtype) {
        persons = new ArrayList<Person>();
        pets = new ArrayList<Pet>();
        sensors = new ArrayList<Sensor>();
        devices = new ArrayList<Device>();
        eventHandler = new EventHandler(this);
        this.roomtype = roomtype;
    }

    public Room() {
        persons = new ArrayList<Person>();
        pets = new ArrayList<Pet>();
        sensors = new ArrayList<Sensor>();
        devices = new ArrayList<Device>();
        eventHandler = new EventHandler(this);
        this.roomtype = ROOMTYPE.Bathroom;
    }


    //Getters
    public ROOMTYPE getRoomtype() {
        return roomtype;
    }

    public EventHandler getEventHandler() {
        return eventHandler;
    }

    public List<Person> getPersons() {
        return persons;
    }

    public List<Pet> getPets() {
        return pets;
    }

    public List<Sensor> getSensors() {
        return sensors;
    }

    public List<Device> getDevices() {
        return devices;
    }

    public int getHumidity() {
        return humidity;
    }

    public int getTemperature() {
        return temperature;
    }

    public int getNaturalLight() {
        return naturalLight;
    }

    public int getTargetTemperature() {
        return targetTemperature;
    }

    public int getTargetHumidity() {
        return targetHumidity;
    }

    //Setters

    public boolean setHumidity(int humidity) {
        if(humidity >= 0 && humidity <= 100) {
            this.humidity = humidity;
            return true;
        }
        return false;
    }

    public boolean setTemperature(int temperature) {
        if(temperature > -273 && temperature < 420) {
            this.temperature = temperature;
            return true;
        }
        return false;
    }

    public boolean setNaturalLight(int naturalLight) {
        if(naturalLight >= 0 && naturalLight <= 100) {
            this.naturalLight = naturalLight;
            return true;
        }
        return false;
    }

    public void setTargetTemperature(int targetTemperature) {
        this.targetTemperature = targetTemperature;
    }

    public void setTargetHumidity(int targetHumidity) {
        this.targetHumidity = targetHumidity;
    }

    public void addPerson(Person person) {
        persons.add(person);
    }

    public void addPet(Pet pet) {
        pets.add(pet);
    }

    public void addSensor(Sensor sensor) {
        sensors.add(sensor);
        sensor.setEventHandler(getEventHandler());
    }

    public void addDevice(Device device) {
        device.setEventHandler(getEventHandler());
        devices.add(device);
    }

    public void removePerson(Person person) {
        persons.remove(person);
    }

    public void removePet(Pet pet) {
        pets.remove(pet);
    }

    @Override
    public String toString() {
        return roomtype.name();
    }
}



