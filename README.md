# OMO_semestral_project

## Decription
Semestral project for subject B211 B6B36OMO.

## Requirements:
##### F1: Entities are devices, sensors, persons, pets and vehicles.
##### F2: Devices that can be used by persons have API PersonUsable which provides methods for controlling them.
##### F3: Devices has parameters idleConsumption and activeConsumption.
##### F4: Devices has an API electricityConsuming, which provides a method getConsumption().
##### F5: In update method of Persons, they decide what they do, which can affect other entities (dse devices or vehicles).
##### F6: Every person is always in a room and can generate events(EmptyFride, DeviceBrokenDown, MotionSensor).
##### F7: Events are recieved by eventHandler, which forwards the event to a correct recipient.
##### F8: Every report can be obtained by calling its method.
##### F9: When a device breaks down a person which is capable of repairing it, recievs an event and procceds to repair it. Repair requires a manual which can be obtained from the device.
##### F10: All persons have 50% chance of using a device and 50% chance of using a vehicle. When there is no awailable device or vehilce, the person waits.

## Used design patterns:
Builder - HouseBuilder, RoomBuilder

Factory - DeviceFactory

LazyInitializaiton - repair manual

StateMachine - DeviceState(TurnOnState, TurnOffState, BrokenState)

Iterator - PersonIterator, DeviceIterator, SensorIterator, PetIterator

## How to use

You create a new Simulation. In parameters you provide a house or json file of houseconfig, temperature, humidity, simulation length and daylength in ticks.

The only thing you have to do in order to statr the simulation is to call the method run().

If you want reports from the simulation, they can be obtained by calling the houseConfigReport(), eventReport(), usageReport() and consumptionReport() method.

House setup .json files are stored in input folde. Reports are stored in output folder.

